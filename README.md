# Rommate App

## Requisitos

- Node.js v12
- Angular 10
- Ionic CLI v5.4.16

## ¿ Cómo ejecutar el proyecto en local ?

```
1.- npm install ó npm i  # Instala las dependencias
2.- ionic serve          # Para ejecurlo en el navegador
3.- ionic cordova run android -c -s -l # Para ejecutarlo en un dispositivo android
```

// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  config : {
    apiKey: "AIzaSyCkok-2rGnxWGnVWeKKkvdkqyxCwioCmbU",
    authDomain: "alohaus-bb221.firebaseapp.com",
    projectId: "alohaus-bb221",
    storageBucket: "alohaus-bb221.appspot.com",
    messagingSenderId: "162694331804",
    appId: "1:162694331804:web:80fc8b70ee7838c8c35c59",
    measurementId: "G-V4C84QWDHF"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
//export const SERVER_URL = 'http://localhost:3000/';
export const SERVER_URL = 'http://localhost:3000/';//'http://192.168.1.111:3000/';//'https://alohaus.herokuapp.com/'//'http://192.168.0.9:3000/';
//export const SERVER_URL = 'https://alohaus.herokuapp.com/'
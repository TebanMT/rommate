export class MessageType {
    public static readonly MSG_REQ: String = "message_request";
    public static readonly MSG_RES: String = "message_response"
}

export interface ChatMessage {
    id_room?: number;
    type: MessageType;
    from: string;
    to: string;
    message: any;
    epoch?:number;
}

export interface UsuarioChat{
    id: number,
    nombre: string;
    apellidos: string;
    url_image?: string;
}

export interface RoomChat{
    id?:number;
    emisor?: UsuarioChat;
    receptor: UsuarioChat;
    messages?: ChatMessage[];
}

export interface ListRoomChat{
    chat_room? : RoomChat;
    messages?  : ChatMessage[];
}
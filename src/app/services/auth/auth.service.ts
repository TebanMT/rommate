import { Injectable } from '@angular/core';
import { BehaviorSubject, throwError, Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { IonItem, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { SERVER_URL } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { NavController } from "@ionic/angular";


export class user {
  nombre: string;
}


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  // API path
  basePath = SERVER_URL;
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

  private currentUserSubject : BehaviorSubject<any>;
  public currentUser         : Observable<any>;

  constructor(private storage: Storage,
    private platform: Platform,
    private router: Router,
    private http: HttpClient,
    private navCtrl: NavController)
    {
      this.currentUserSubject = new BehaviorSubject<any>(null);
      this.currentUser = this.currentUserSubject.asObservable();
      this.init();
    }

  init(){
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }

    /**
   * Manejador de errores
   * @param error
   */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.log(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error)
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };


  ifLoggedIn() {
    this.storage.get('USER_INFO').then((response) => {
      if (response) {
        this.currentUserSubject.next(response);
      }else{
        this.currentUserSubject.next(null);
      }
    });
  }

  login(data): Observable<any>{
    return this.http
      .post<any>(this.basePath+'api/user/login', JSON.stringify(data), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  saveDataLogin(data){
    this.storage.set('USER_INFO', data).then((response) => {
      this.currentUserSubject.next(data);
      this.navCtrl.navigateRoot('/');
    });
  }

  updateUser(key, value){
    var the_user = this.currentUserValue;
    the_user.payload[key] = value;
    this.saveDataLogin(the_user);
  }

  logout() {
    this.storage.remove('USER_INFO').then(() => {
      this.currentUserSubject.next(null);
      this.navCtrl.navigateRoot('/');
    });
  }

  public get currentUserValue(){
    return this.currentUserSubject.value;
  }

}

import { Injectable } from '@angular/core';
import { AuthService } from "./auth.service";
import { Router } from '@angular/router';
import { NavController } from "@ionic/angular";

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private auth: AuthService, private router: Router, private navCtrl: NavController) { }

  canActivate(): boolean {
    if (this.auth.currentUserValue ==null) {
      alert("Necesita Iniciar sesion")
      this.navCtrl.navigateRoot('/tabs/profile')
      return false;
    }
    return true;
  }
}

import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

class Photo {
  data: any;
  tipo: any;
}

@Injectable({
  providedIn: 'root'
})
export class PhotoService {

  public photos: Photo[] = [];

  constructor(private camera: Camera) { }

  takePicture() {
    const options: CameraOptions = {
      quality: 80,
      targetWidth: 1280,
      targetHeight: 1280,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation : true,
    };

    this.camera.getPicture(options).then((imageData) => {
      this.photos.unshift({
        data: 'data:image/jpeg;base64,' + imageData,
        tipo: "PHOTO",
    });
    }, (err) => {
      // Handle error
      console.log("Camera issue:" + err);
    });
  }

  openGallery() {
    const gelleryOptions: CameraOptions = {
      quality: 80,
      targetWidth: 1280,
      targetHeight: 1280,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      destinationType: this.camera.DestinationType.DATA_URL,
      allowEdit: true,
      correctOrientation : true
    }
    this.camera.getPicture(gelleryOptions).then((imgData) => {
     this.photos.unshift({
      data: 'data:image/jpeg;base64,' + imgData,
      tipo: "PHOTO"
    });
     //this.base64Img = 'data:image/jpeg;base64,' + imgData;
     //this.userImg = this.base64Img;
     }, (err) => {
     console.log(err);
     })
    }

    restorePhotos(){
      this.photos = [];
    }

}

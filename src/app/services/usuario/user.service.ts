import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SERVER_URL } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  // API path
  basePath = SERVER_URL;
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

  constructor(private http: HttpClient) { }

  /**
   * Manejador de errores
   * @param error
   */
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.log(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error)
    }
    // return an observable with a user-facing error message
    return throwError(
      'El Correo electronico ya existe, use uno diferente');
  };

  /**
   *  Crea un nuevo usuario
   * @param user
   */
  newUser(user): Observable<any> {
    return this.http
      .post<any>(this.basePath+'api/user', JSON.stringify(user), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  /**
   * 
   */
  find(id: number): Observable<any>{
    return this.http
    .get<any>(this.basePath+'api/user/'+id, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

}

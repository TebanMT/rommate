import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { SERVER_URL } from '../../../environments/environment';
import { ChatMessage, MessageType } from '../models/chat-model';
import { UserService } from '../usuario/user.service';
import { UtilService } from '../util/util.service';
import { DatabaseService } from '../database/database.service';
import { Subject, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { io, Socket } from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class MensajesService {

  private socket: Socket;

  private socketObserver: any;
  public messages: any;

  constructor(public databaseService: DatabaseService,
    private util: UtilService, private user: UserService) {
    this.socketObserver = new Subject<any>();
    this.messages = this.socketObserver.asObservable();
  }

  init(user) {
    console.log(user)
    this.socket = io('http://localhost:3001', {query: {user: user},autoConnect: true});
    this.socket.on("connectt", (id) => {
      console.log('***Socket Connected***');
      //this.socket.emit('get_messages_not_recived', id);
    });

    this.socket.on("reconnecting", attempt => {
      console.log("***Socket Reconnecting***")
      console.debug('***Socket Reconnecting***', attempt);
    });

    this.socket.on("reconnect_failed", () => {
      console.debug('***Socket Reconnect failed***');
    });

    this.socket.on('disconnect', () => {
      console.debug('***Socket Disconnected***');
    });

    this.socket.on(MessageType.MSG_RES.toString(), response => {
      console.log("-------recivido--------------")
      const chatMessage: ChatMessage = {
        type: MessageType.MSG_RES,
        to: response.message.to,
        from: response.message.from,
        message: response.message.message,
        epoch: UtilService.getEpoch()
      };
      this.reciveMessage(chatMessage, response.id);
      /*this.user.find(response.from).subscribe(user => {
        this.databaseService.getJson(response.from, "messages")
        .then(async messages => {
          console.log(messages)
          if (messages === null) {
            messages = [];
            const room_chat = await this.util.formatNewChatRoom(user.payload, response);
            await this.databaseService.setJson(response.from,room_chat,'chat_rooms')
          }
          messages.push(chatMessage);
          return this.databaseService.setJson(response.from, messages, "messages");
      })
      .then(success => {
        if (success) {
          this.socketObserver.next(chatMessage);
        }
      })
      .catch(error => {
        console.log(error);
      });
      });*/
    });
  }



  sendMessage(idChat: string,chatMessage: ChatMessage){
    chatMessage.epoch = UtilService.getEpoch();
    this.databaseService.getJson(idChat, "mensajes")
      .then(messages => {
        if (!messages) {
          messages = [];
        }
        messages.push(chatMessage);
        return this.databaseService.setJson(idChat, messages, 'mensajes');
      })
      .then(success => {
        if (success) {
          this.socketObserver.next(chatMessage);
          this.socket.emit(MessageType.MSG_REQ.toString(), chatMessage);
        }
      });
  }

  reciveMessage(message: ChatMessage, id: any){
      this.databaseService.getJson(message.from, "mensajes")
        .then(async messages => {
          if (messages === null) {
            messages = [];
            this.user.find(parseInt(message.from)).subscribe(async user => {
              const room_chat = await this.util.formatNewChatRoom(user.payload, message.to);
              await this.databaseService.setJson(message.from,room_chat,'sala')
            })
          }
          messages.push(message);
          return this.databaseService.setJson(message.from, messages, "mensajes");
      })
      .then(async success => {
        if (success) {
          this.socketObserver.next(message);
          await this.databaseService.getAllChatRoomsJson("sala");
          //this.databaseService.chatRoomObserver.next(message);
          if (id) {
            this.socket.emit("received", id);
          }
        }
      })
      .catch(error => {
        console.log(error);
      });
    
  }


  connect(user) {
    this.init(user)
    this.socket.connect();
  }

  disconnect() {
    this.socket.disconnect();
  }

}













/**
 * 
 *   // API path
  basePath = SERVER_URL;
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

  nofity = {
    message:{
        notification:{
        title:"Te han enviado un mensaje",
        body:"",
        sound:"default",
        click_action:"FCM_PLUGIN_ACTIVITY",
        icon:"fcm_push_icon"
    },
    data:{
        id_usuario_origen: "",
        id_usuario_destino: "",
        mensaje: "",
        tipo: "Text",
        fecha_dipositivo: ""
    }
  },
   to: "",
   priority:"high",
   restricted_package_name:""
  }

  private socketObserver: any;
  public messages: any;

  constructor(private http: HttpClient, public databaseService: DatabaseService,
    private util: UtilService, private user: UserService) {
    this.socketObserver = new Subject<any>();
    this.messages = this.socketObserver.asObservable();
  }

  // Handle API errors
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.log(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error)
    }
    // return an observable with a user-facing error message
    return throwError(
      error);
  };

  sendMessage(idChat: string,chatMessage: ChatMessage){
    chatMessage.epoch = UtilService.getEpoch();
    console.log(chatMessage)
    this.nofity.message.notification.body=chatMessage.message
    this.nofity.message.data.id_usuario_origen=chatMessage.from
    this.nofity.message.data.id_usuario_destino=chatMessage.to
    this.nofity.message.data.mensaje=chatMessage.message
    this.nofity.message.data.fecha_dipositivo=chatMessage.epoch.toString()
    this.databaseService.getJson(idChat, "mensajes")
      .then(messages => {
        if (!messages) {
          messages = [];
        }
        messages.push(chatMessage);
        return this.databaseService.setJson(idChat, messages, 'mensajes');
      })
      .then(success => {
        if (success) {
          this.socketObserver.next(chatMessage);
          this.http.post<any>(this.basePath+'api/notify/', this.nofity, this.httpOptions)
          .pipe(
            catchError(this.handleError)
          ).subscribe(res=>{
            console.log(res)
          },err=>{
            console.log(err)
          })
        }
      });
  }

  reciveMessage(background: any, message: ChatMessage){
    this.user.find(parseInt(message.from)).subscribe(user => {
      this.databaseService.getJson(message.from, "mensajes")
        .then(async messages => {
          console.log(messages)
          if (messages === null) {
            messages = [];
            const room_chat = await this.util.formatNewChatRoom(user.payload, message.to);
            await this.databaseService.setJson(message.from,room_chat,'sala')
          }
          messages.push(message);
          return this.databaseService.setJson(message.from, messages, "mensajes");
      })
      .then(success => {
        if (success) {
          this.socketObserver.next(message);
        }
      })
      .catch(error => {
        console.log(error);
      });
    })
  }
 */
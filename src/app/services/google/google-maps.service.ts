import { Injectable } from '@angular/core';
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { ElementRef, NgZone } from "@angular/core";
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GoogleMapsService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'text/plain; charset=utf-8'
    })
  };

  constructor(private http: HttpClient) { }


}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SERVER_URL } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SmsService {

  // API path
  basePath = SERVER_URL;
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

  constructor(private http: HttpClient) { }

  // Handle API errors
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.log(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error)
    }
    // return an observable with a user-facing error message
    return throwError(
      error);
  };

  // Create a new code sms
  sendSMS(item): Observable<any> {
    return this.http
      .post<any>(this.basePath+'api/sms/', JSON.stringify(item), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  /**
   * 
   * @param dataToVerify 
   */
  verifyCode(dataToVerify): Observable<any> {
    return this.http
      .post<any>(this.basePath+'api/sms/verify', JSON.stringify(dataToVerify), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

}

import { Injectable } from '@angular/core';
import * as moment from "moment";
import {ChatMessage, ListRoomChat, MessageType, RoomChat, UsuarioChat} from "../models/chat-model";
import { AuthService } from "../auth/auth.service";
import { UserService } from "../usuario/user.service";

moment.locale('es-es');

@Injectable({
  providedIn: 'root'
})
export class UtilService {

  constructor(private auth: AuthService, private user: UserService) { }

  static getEpoch(date?:string): number {
    const result = date ? moment(date).unix() : moment().unix();
    return result;
  }

  static getCalendarDay(epoch: number): string {
    if (!epoch) {
      return null;
    }
    let timeString = 'h:mm A';
    return moment(epoch * 1000).calendar(null, {
      sameDay: '[Today] ' + timeString,
      lastDay: '[Yesterday] ' + timeString,
      sameElse: 'MM/DD ' + timeString
    });
  }

  static getMonthYear(epoch: number): string {
    return moment(epoch*1000).format("MMMM, Y")
  }

  static formatMessageRequest(message: string, from: string, to: string): ChatMessage {
    return {
      type: MessageType.MSG_REQ,
      to: to,
      from: from,
      message: message
    };
  }

  formatEmisorMessages(id_emsor){
    return new Promise<any>((resolve, reject) => {
      const currentUser = this.auth.currentUserValue;
      if(currentUser == null){
        console.log("asdasd")
        this.user.find(id_emsor).subscribe(res=>{
          console.log(res)
          const emisor: UsuarioChat = {
            id: res.id,
            nombre: res.nombre,
            apellidos: res.apellidos,
            url_image: '',
          }
          resolve(emisor);
        },err=>{
          console.log(err)
          resolve(null)
        });
      }else{
        const emisor: UsuarioChat = {
          id: currentUser.payload.id,
          nombre: currentUser.payload.nombre,
          apellidos: currentUser.payload.apellidos,
          url_image: '',
        }
        resolve(emisor);
      }
    })
  }

  async formatNewChatRoom(from_user: any, id_emsor: any){
    const emisor = await this.formatEmisorMessages(id_emsor);
    const receptor = from_user;
    const dataChatRoom: RoomChat = {
      id       : receptor.id,
      receptor : {id: receptor.id, nombre: receptor.nombre, apellidos: receptor.apellidos, url_image: receptor.url_image},
      emisor   : {id: emisor.id, nombre: emisor.nombre, apellidos: emisor.apellidos, url_image: emisor.url_image},
    }
    return dataChatRoom;
  }

}

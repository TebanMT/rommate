import { TestBed } from '@angular/core/testing';

import { EspacioRentaService } from './espacio-renta.service';

describe('EspacioRentaService', () => {
  let service: EspacioRentaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EspacioRentaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

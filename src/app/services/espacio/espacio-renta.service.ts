import { Injectable } from '@angular/core';
import { SERVER_URL } from '../../../environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EspacioRentaService {

  // API path
  basePath = SERVER_URL;
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  };

  constructor(private http: HttpClient,) { }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.log(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error)
    }
    // return an observable with a user-facing error message
    return throwError(
      error);
  };

  createEspacio(data): Observable<any>{
    return this.http
      .post<any>(this.basePath+'api/espacio', JSON.stringify(data), this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  getEspacios(): Observable<any> {
    return this.http
    .get<any>(this.basePath+'api/espacio',this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  getEspacio(id: number): Observable<any> {
    return this.http
    .get<any>(this.basePath+'api/espacio/'+id,this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  searchEspacios(lugar?:string, tipo?:string, fecha?:string): Observable<any> {
    return this.http
    .get<any>(this.basePath+'api/espacio/'+lugar+'/'+tipo+'/'+fecha+'/null',this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  searchEspaciosByAnfitrion(id_usuario:string): Observable<any> {
    return this.http
    .get<any>(this.basePath+'api/espacio/null/null/null/'+id_usuario,this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  getCountDireccion(): Observable<any> {
    return this.http
    .get<any>(this.basePath+'api/espacio/cities/',this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  deleteEspacio(id:number): Observable<any>{
    return this.http.delete<any>(this.basePath+'api/espacio/'+id, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

  changeActivo(id:number, status: boolean): Observable<any>{
    return this.http.patch<any>(this.basePath+'api/espacio/',{id:id,status:status}, this.httpOptions)
    .pipe(
      catchError(this.handleError)
    )
  }

}

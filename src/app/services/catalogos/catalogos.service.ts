import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { SERVER_URL } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CatalogosService {

  // API path
  basePath = SERVER_URL;
  // Http Options
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    })
  }

  constructor(private http: HttpClient) { }

  // Handle API errors
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.log('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.log(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
      console.log(error)
    }
    // return an observable with a user-facing error message
    return throwError(
      error);
  };


  getEstados(): Observable<any> {
    return this.http
      .get<any>(this.basePath+'api/catalogos/estados', this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  getMunicipio(idEstado): Observable<any> {
    return this.http
      .get<any>(this.basePath+'api/catalogos/municipios/'+idEstado, this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  getEspacios(): Observable<any> {
    return this.http
      .get<any>(this.basePath+'api/catalogos/espacios/', this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  getServicios(): Observable<any> {
    return this.http
      .get<any>(this.basePath+'api/catalogos/servicios', this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  getSeguridadPriv(): Observable<any> {
    return this.http
      .get<any>(this.basePath+'api/catalogos/seguridad', this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

  getReglas(): Observable<any> {
    return this.http
      .get<any>(this.basePath+'api/catalogos/reglas', this.httpOptions)
      .pipe(
        catchError(this.handleError)
      )
  }

}

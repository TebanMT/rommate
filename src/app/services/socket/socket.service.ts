import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { io, Socket } from "socket.io-client";
import { DatabaseService } from '../database/database.service';
import { ChatMessage, MessageType, RoomChat } from '../models/chat-model';
import { UserService } from '../usuario/user.service';
import { UtilService } from '../util/util.service';


@Injectable({
  providedIn: 'root'
})
export class SocketService {

  private socketObserver: any;
  public messages: any;
  private socket: Socket;

  constructor(public databaseService: DatabaseService,private user: UserService, private util: UtilService){
      this.socketObserver = new Subject<any>();
      this.messages = this.socketObserver.asObservable();
  }

  /*init(user) {
    console.log(user)
    this.socket = io('http://localhost:3001', {query: {user: user},autoConnect: true});
    this.socket.on("connectt", () => {
      console.log('***Socket Connected***');
    });

    this.socket.on("reconnecting", attempt => {
      console.debug('***Socket Reconnecting***', attempt);
    });

    this.socket.on("reconnect_failed", () => {
      console.debug('***Socket Reconnect failed***');
    });

    this.socket.on('disconnect', () => {
      console.debug('***Socket Disconnected***');
    });

    this.socket.on(MessageType.MSG_RES.toString(), response => {
      this.user.find(response.from).subscribe(user => {
        const chatMessage: ChatMessage = {
          type: MessageType.MSG_RES,
          to: response.from,
          from: response.to,
          message: response.message,
          epoch: UtilService.getEpoch()
        };
        this.databaseService.getJson(response.from, "messages")
        .then(async messages => {
          console.log(messages)
          if (messages === null) {
            messages = [];
            const room_chat = await this.util.formatNewChatRoom(user.payload, response);
            await this.databaseService.setJson(response.from,room_chat,'chat_rooms')
          }
          messages.push(chatMessage);
          return this.databaseService.setJson(response.from, messages, "messages");
      })
      .then(success => {
        if (success) {
          this.socketObserver.next(chatMessage);
        }
      })
      .catch(error => {
        console.log(error);
      });
      });
    });
  }

  newRequest(idChat: string,chatMessage: ChatMessage) {
    chatMessage.epoch = UtilService.getEpoch();
    this.databaseService.getJson(idChat, "messages")
      .then(messages => {
        if (!messages) {
          messages = [];
        }
        messages.push(chatMessage);
        return this.databaseService.setJson(idChat, messages, 'messages');
      })
      .then(success => {
        if (success) {
          this.socketObserver.next(chatMessage);
          this.socket.emit(MessageType.MSG_REQ.toString(), chatMessage);
        }
      });
  }

  connect(user) {
    this.init(user)
    this.socket.connect();
  }

  disconnect() {
    this.socket.disconnect();
  }*/

}

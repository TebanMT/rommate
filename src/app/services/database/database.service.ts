import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { RoomChat } from '../models/chat-model';
import { SqlService } from "./sql.service";

@Injectable({
  providedIn: 'root'
})
export class DatabaseService {

  private chatRoomObserver : Subject<any>;
  public chatRooms: Observable<any>;

  constructor(public _db: SqlService) {
    this.chatRoomObserver = new Subject<any>();
    this.chatRooms = this.chatRoomObserver.asObservable();
  }

  //
  // Shared getter setter
  //
  private set(key: string, value: string, table: string): Promise<boolean> {
    return this._db.setJson(key, value, table)
      .then((res) => true)
      .catch(err => {
        console.error(err)
        console.error('[Error] Saving ' + key + ' - ' + JSON.stringify(err));
        return false;
      });
  }

  private get(key?: string, table?: string): Promise<string> {
    return this._db.getJson(key, table)
      .then(value => {
        if (value) {
          return value;
        } else {
          throw new Error('Undefined value');
        }
      })
      .catch(err => {
        console.error('[Error] Getting ' + key + ' - ' + JSON.stringify(err)+' - No hay '+table);
        return null;
      });
  }

  remove(key: string): Promise<boolean> {
    return this._db.remove(key)
      .then(() => true)
      .catch(err => {
        console.error('[Error] Removing ' + key + ' - ' + JSON.stringify(err));
        return false;
      });
  }

  getJson(key: string, table: string): Promise<any> {
    return this.get(key, table).then(value => {
      try {
        return JSON.parse(value);
      } catch (err) {
        console.error('Storage getJson(): unable to parse value for key', key, ' as JSON');
        return null;
      }
    });
  }

  getAllChatRoomsJson(table: string, type?:string): Promise<any>{
    return this.get(undefined,table).then((value:any) => {
      try {
        for (const iterator of value){
          const i = value.indexOf(iterator)
          if (iterator.messages[0].type == type || !type) {
            iterator.messages = [iterator.messages[iterator.messages.length - 1]]
          }else{
            console.log(iterator.messages[0])
            value.splice(i,i+1)
            console.log(value)
          }
        }
        this.chatRoomObserver.next(value);
        return value;
      } catch (err) {
        console.error('Storage getJson(): unable to parse values as JSON - No hay salas con mensajes');
        return null;
      }
    });
  }

  async setJson(key: string, value: any, table: string): Promise<boolean> {
    try {
      const result = await this.set(key, JSON.stringify(value), table);
      if (result && table == 'mensajes' && value.length == 1) {
        const r = await this.getAllChatRoomsJson('sala');
      }
      return result;
    } catch (err) {
      return Promise.resolve(false);
    }
  }

}

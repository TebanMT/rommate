import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { SignUpPageModule } from "./modules/sign-up/sign-up.module";
import { DetailsPageModule } from "./modules/reservacion/details/details.module";
import { SearchPageModule } from "./modules/tabs/search/search.module";
import { SerachDatePageModule } from "./modules/tabs/search/serach-date/serach-date.module";
import { SerachTypePageModule } from "./modules/tabs/search/serach-type/serach-type.module";
import { RegisterPlacePageRoutingModule } from "./modules/register-place/register-place-routing.module";
import { IonicStorageModule } from '@ionic/storage';
//import { NgCalendarModule  } from 'ionic2-calendar';

//Providers
import { SmsService } from "./services/sms/sms.service";
import { AuthService } from './services/auth/auth.service';
import { AuthGuardService } from "./services/auth/auth-guard.service";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { GoogleMapsService } from './services/google/google-maps.service'
import { CatalogosService } from "./services/catalogos/catalogos.service";
import { PhotoService } from "./services/device/photo.service";
import { EspacioRentaService } from "./services/espacio/espacio-renta.service";
import { UtilService } from "./services/util/util.service";
import { SqlService } from "./services/database/sql.service";
import { DatabaseService } from "./services/database/database.service";
import { SocketService } from "./services/socket/socket.service";
import { MensajesService } from "./services/mensajes/mensajes.service";

import { SocketIoModule, SocketIoConfig, Socket } from "ngx-socket-io";
//import { FCM } from "@ionic-native/fcm/ngx";

//import { Socket } from "socket.io-client";

const config: SocketIoConfig = { url: 'http://192.168.1.113:3001' };


import { Camera } from '@ionic-native/camera/ngx';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    FormsModule,
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    HttpClientModule,
    SignUpPageModule,
    DetailsPageModule,
    SearchPageModule,
    SerachTypePageModule,
    SerachDatePageModule,
    RegisterPlacePageRoutingModule,
    IonicStorageModule.forRoot(),
    SocketIoModule.forRoot(config),
    //NgCalendarModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SmsService,
    AuthService,
    AuthGuardService,
    Geolocation,
    CatalogosService,
    GoogleMapsService,
    Camera,
    PhotoService,
    EspacioRentaService,
    UtilService,
    SqlService,
    DatabaseService,
    SocketService,
    MensajesService,
    //sFCM,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

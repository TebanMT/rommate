import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from "./services/auth/auth-guard.service";

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./modules/tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'register-place',
    canActivate : [AuthGuardService],
    loadChildren: () => import('./modules/register-place/register-place.module').then( m => m.RegisterPlacePageModule)
  },
  {
    path: 'full-description/:id',
    loadChildren: () => import('./modules/reservacion/full-description/full-description.module').then( m => m.FullDescriptionPageModule)
  },
  {
    path: 'chat/:id',
    canActivate : [AuthGuardService],
    loadChildren: () => import('./modules/chat/room-chat/room-chat.module').then( m => m.RoomChatPageModule)
  },
  {
    path: 'anfitrion',
    loadChildren: () => import('./modules/anfitrion/tabs/tabs.module').then( m => m.TabsPageModule)
  },
  //{
  //  path: 'search',
  //  loadChildren: () => import('./modules/tabs/search/serach-result/serach-result.module').then( m => m.SerachResultPageModule)
  //},
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

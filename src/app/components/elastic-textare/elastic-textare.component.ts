import { Component, ElementRef, OnInit, ViewChild, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-elastic-textare',
  templateUrl: './elastic-textare.component.html',
  inputs: ['placeholder', 'lineHeight', 'maxExpand'],
  styleUrls: ['./elastic-textare.component.scss'],
})
export class ElasticTextareComponent implements OnInit {

  @ViewChild('ionTxtArea', {read: ElementRef }) ionTxtArea: any;
  public txtArea: any;
  public content: string;
  public lineHeight: number;
  public placeholder: string;
  public maxHeight: number;
  public maxExpand: number;

  constructor(private renderer: Renderer2) {
    this.content = "";
    this.lineHeight = 30;
    this.maxExpand = 5;
    this.maxHeight = this.lineHeight * this.maxExpand;
  }

  public ngAfterViewInit() {
    this.txtArea = this.ionTxtArea.nativeElement;
    this.txtArea.style.height = this.lineHeight + "px";
    this.maxHeight = this.lineHeight * this.maxExpand;
    this.txtArea.style.resize = 'none';
  }

  public onChange(event) {
    this.txtArea.style.height = this.lineHeight + "px";
    if (this.txtArea.scrollHeight < this.maxHeight) {
      this.txtArea.style.height = this.txtArea.scrollHeight + "px";
    } else {
      this.txtArea.style.height = this.maxHeight + "px";
    }
  }

  public clearInput() {
    this.content = "";
    this.txtArea.style.height = this.lineHeight + "px";
  }

  public setFocus() {
    this.ionTxtArea.setFocus()
  }

  ngOnInit() {}

}

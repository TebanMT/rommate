import { Component, OnInit, OnChanges, Input } from '@angular/core';
import {  UtilService } from "../../services/util/util.service";
import { ChatMessage, MessageType } from "../../services/models/chat-model";

@Component({
  selector: 'app-chat-bubble',
  templateUrl: './chat-bubble.component.html',
  styleUrls: ['./chat-bubble.component.scss'],
})
export class ChatBubbleComponent implements OnInit{

  @Input() chatMessage: ChatMessage;
  public messageType = MessageType;

  constructor() { }

  ngOnInit() {
    /*console.log(this.chatMessage)
    if(this.chatMessage?.type === this.messageType.MSG_REQ){
      console.log('right')
    }*/
  }

  formatEpoch(epoch): string {
    return UtilService.getCalendarDay(epoch);
  }

}

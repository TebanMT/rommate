import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { UserService } from "../../../services/usuario/user.service";
import { ActivatedRoute } from "@angular/router";
import { AuthService } from "../../../services/auth/auth.service";
import { SocketService } from 'src/app/services/socket/socket.service';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';

@Component({
  selector: 'app-form-register',
  templateUrl: './form-register.page.html',
  styleUrls: ['./form-register.page.scss'],
})
export class FormRegisterPage implements OnInit {

  //Fomulario para los datos de nuevo usuario
  nuevoUsario = new FormGroup({
    nombre     : new FormControl('',[Validators.required]),
    token_fcm  : new FormControl(''),
    apellidos  : new FormControl('',[Validators.required]),
    fecha_nacimieto  : new FormControl('',[Validators.required]),
    email      : new FormControl('',[Validators.required]),
    password   : new FormControl('',[Validators.required]),
    lada_int   : new FormControl('',[Validators.required]),
    usuario    : new FormControl(true),
    anfitrion  : new FormControl(false),
    id_tipo_registro  : new FormControl(1),
    numero_cel : new FormControl('',[Validators.required,
                                    Validators.maxLength(10),
                                    Validators.minLength(10),
                                    Validators.pattern(/^\d+$/)
    ]),
  });

  constructor(
    public userController: UserService,
    private route: ActivatedRoute,
    public auth: AuthService
    ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.nuevoUsario.patchValue({
        lada_int   : params["lada_int"],
        numero_cel : params["numero_cel"]
      });
    });
  }

  register(){
    this.nuevoUsario.value.fecha_nacimieto = this.nuevoUsario.value.fecha_nacimieto.split('T')[0];
    FCM.getToken().then(token => {
      //console.log(token);
      this.nuevoUsario.value.token_fcm = token;
      this.userController.newUser(this.nuevoUsario.value)
      .subscribe(
        (res) => {
          console.log(res);
          this.auth.saveDataLogin(res);
        },
        (err) => {
          console.log(err);
          alert(err);
        })
    }).catch(err =>{
      alert(err)
    });
  }

}

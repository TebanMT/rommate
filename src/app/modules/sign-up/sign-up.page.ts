import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { ModalController, NavController } from "@ionic/angular";
import { SmsService } from "../../services/sms/sms.service";
import { NavigationExtras } from '@angular/router';
import { AuthService } from "../../services/auth/auth.service";


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {

  //Fomulario para el campo de numero de telefono
  numberPhone = new FormGroup({
    lada_int   : new FormControl('',[Validators.required]),
    tipo       : new FormControl(''),
    numero_cel : new FormControl('',[Validators.required,
      Validators.maxLength(10),
      Validators.minLength(10),
      Validators.pattern(/^\d+$/)
    ]),
  });

  // bandera para conocer si el usuario se logeara o se registrara
  login: boolean;
  textBanner: String;
  textChangeMode: String;
  textLinkChangeMode: String;
  textButtonMode: String;
  navigationExtras: NavigationExtras;
  dataUser: any;

  constructor(public modalCtrl:ModalController,
    private smsCtrl:SmsService,
    private navController: NavController,
    private auth: AuthService) { }

  ngOnInit() {
    this.changeText()
  }

  /**
   * 
   */
  logIn(){
    this.auth.login(this.numberPhone.value)
    .subscribe(
      res =>{
        console.log(res);
        if (res == null) {
          this.numberPhone.patchValue({tipo:"SingUp"});
          this.login = false;
          alert("El numero que ingreso no está registrado")
          this.sendSMS()
        }else{
          this.dataUser = res;
          this.login = true;
          this.numberPhone.patchValue({tipo:"LogIn"});
          this.sendSMS()
          alert("El numero ya esta registrado")
        }
      },
      err =>{
        alert("Error Desconocido al verificar usuario: "+err)
      }
    );
  }

  sendSMS(){
    this.smsCtrl.sendSMS(this.numberPhone.value)
    .subscribe(
      _ => {
        this.setNavigationParams();
        this.navController.navigateForward('/validate', this.navigationExtras);
        this.close();
      },
      (err) => {
        if (err.error.error.code == 21211 || err.error.error.code == 21614 || err.error.error.code == 21608) {
          alert('Numero de telefono invalido ');
        }else{
          alert('Error desconocido al enviar sms, intentelo más tarde');
        }
      }
    )
  }

  setNavigationParams(){
    this.navigationExtras = {
      queryParams: {
          numberPhone: this.numberPhone.value,
          login : this.login,
          dataUser : this.dataUser
      }
    };
  }

  /**
   * Cambia a modo Log in o Sign Up
   */
  changeMode(){
    this.login = !this.login;
    this.changeText()
  }

  /**
   * Cambio de los textos en la pagina
   */
  changeText(){
    this.textBanner = this.login ? "Inicia Sesion" : "Unete a nuestra comunidad";
    this.textChangeMode = this.login ? "¿No tienes cuenta?" : "¿Ya tienes cuenta?";
    this.textLinkChangeMode = this.login ? "Sign Up" : "Log In";
    this.textButtonMode = this.login ? "Log In" : "Sign Up";
    let tipoCode = this.login ? "LogIn" : "SingUp";
    this.numberPhone.patchValue({tipo:tipoCode});
  }

  /**
   * Close Modal
   */
  close(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}

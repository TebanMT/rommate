import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ValidateCodeSmsPageRoutingModule } from './validate-code-sms-routing.module';

import { ValidateCodeSmsPage } from './validate-code-sms.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ValidateCodeSmsPageRoutingModule
  ],
  declarations: [ValidateCodeSmsPage]
})
export class ValidateCodeSmsPageModule {}

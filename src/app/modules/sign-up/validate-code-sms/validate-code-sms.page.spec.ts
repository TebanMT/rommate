import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ValidateCodeSmsPage } from './validate-code-sms.page';

describe('ValidateCodeSmsPage', () => {
  let component: ValidateCodeSmsPage;
  let fixture: ComponentFixture<ValidateCodeSmsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateCodeSmsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ValidateCodeSmsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

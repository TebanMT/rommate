import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup } from '@angular/forms';
import { SmsService } from "../../../services/sms/sms.service";
import { NavController } from "@ionic/angular";
import { NavigationExtras } from '@angular/router';
import { AuthService } from "../../../services/auth/auth.service";
import { SocketService } from 'src/app/services/socket/socket.service';

@Component({
  selector: 'app-validate-code-sms',
  templateUrl: './validate-code-sms.page.html',
  styleUrls: ['./validate-code-sms.page.scss'],
})
export class ValidateCodeSmsPage implements OnInit {

  dataToVerify: any;
  dataUser:any;
  login: boolean;
  //Fomulario para el campo de numero de telefono
  codigo = new FormGroup({
    d1: new FormControl(''),
    d2: new FormControl(''),
    d3: new FormControl(''),
    d4: new FormControl('')
  });

  @ViewChild('Field1', { static: true }) Field1;
  @ViewChild('Field4', { static: true }) Field4;

  constructor(private route: ActivatedRoute,
    private smsService: SmsService,
    private navController: NavController,
    private auth: AuthService,
    private socketService: SocketService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.dataToVerify = params["numberPhone"];
      this.login = params["login"];
      this.dataUser = params["dataUser"]
    });
  }

  gotoNextField(nextElement){
    nextElement.setFocus();
  }

  finishFunction(){
    let auxCodigo = this.codigo.value;
    let redirect = this.login ? "/" : "/register";
    this.dataToVerify['codigo'] = auxCodigo.d1+auxCodigo.d2+auxCodigo.d3+this.Field4.value;
    this.dataToVerify['numero_telefono'] = this.dataToVerify['lada_int']+this.dataToVerify['numero_cel'];
    this.smsService.verifyCode(this.dataToVerify)
    .subscribe(
      res =>{
        if (res.status) {
          let navigationExtras: NavigationExtras = {
            queryParams: {
                lada_int   : this.dataToVerify['lada_int'],
                numero_cel : this.dataToVerify['numero_cel'],
            }
          };
          if (this.login) {
            this.auth.saveDataLogin(this.dataUser);
          }
          this.navController.navigateForward(redirect, navigationExtras);
        }else{
          this.codigo.setValue({d1:'',d2:'',d3:'',d4:''});
          this.gotoNextField(this.Field1);
        }
      },
      err =>{alert(err);},
      () =>{}
    )
  }

}

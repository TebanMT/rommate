import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlaceReglasPage } from './place-reglas.page';

const routes: Routes = [
  {
    path: '',
    component: PlaceReglasPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlaceReglasPageRoutingModule {}

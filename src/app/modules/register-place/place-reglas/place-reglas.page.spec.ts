import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlaceReglasPage } from './place-reglas.page';

describe('PlaceReglasPage', () => {
  let component: PlaceReglasPage;
  let fixture: ComponentFixture<PlaceReglasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceReglasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlaceReglasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlaceReglasPageRoutingModule } from './place-reglas-routing.module';

import { PlaceReglasPage } from './place-reglas.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlaceReglasPageRoutingModule
  ],
  declarations: [PlaceReglasPage]
})
export class PlaceReglasPageModule {}

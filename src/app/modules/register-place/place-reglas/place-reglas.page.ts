import { Component, OnInit } from '@angular/core';
import { CatalogosService } from "../../../services/catalogos/catalogos.service";
import { NavController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";


@Component({
  selector: 'app-place-reglas',
  templateUrl: './place-reglas.page.html',
  styleUrls: ['./place-reglas.page.scss'],
})
export class PlaceReglasPage implements OnInit {

  selectedReglas: any = [];

  reglas: any;
  newPlace: any;
  navigationExtras:any;

  constructor(private catalogos: CatalogosService,
    private navController: NavController,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.catalogos.getReglas().subscribe(
      res =>{
        this.reglas = res.payload;
        this.reglas.map(r => r.activo = 0);
      },
      err =>{
        alert(err);
      }
    );
    this.route.queryParams.subscribe(params => {
      this.newPlace = params['newPlace'];
    });
  }

  select(s: any){
    if (s.activo == !true) {
      this.selectedReglas.push({id:s.id});
      this.newPlace.controls['reglas'].setValue(this.selectedReglas);
    }else{
      const index = this.selectedReglas.findIndex(i => i.id = s.id);
      if (index > -1) {
        this.selectedReglas.splice(index, 1);
      }
    }
  }

  continue(){
    //console.log(this.selectedReglas)
    this.setNavigationParams();
    this.navController.navigateForward('/place-photos',this.navigationExtras);
  }

  setNavigationParams(){
    this.navigationExtras = {
      queryParams: {
          newPlace: this.newPlace,
      }
    };
  }

}

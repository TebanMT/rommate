import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegisterPlacePageRoutingModule } from './register-place-routing.module';

import { RegisterPlacePage } from './register-place.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RegisterPlacePageRoutingModule
  ],
  declarations: [RegisterPlacePage]
})
export class RegisterPlacePageModule {}

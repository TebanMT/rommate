import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegisterPlacePage } from './register-place.page';

const routes: Routes = [
  {
    path: '',
    component: RegisterPlacePage
  },
  {
    path: 'ubicacion',
    loadChildren: () => import('./ubicacion/ubicacion.module').then( m => m.UbicacionPageModule)
  },
  {
    path: 'place-description',
    loadChildren: () => import('./place-description/place-description.module').then( m => m.PlaceDescriptionPageModule)
  },
  {
    path: 'place-description2',
    loadChildren: () => import('./place-description2/place-description2.module').then( m => m.PlaceDescription2PageModule)
  },
  {
    path: 'place-services',
    loadChildren: () => import('./place-services/place-services.module').then( m => m.PlaceServicesPageModule)
  },
  {
    path: 'place-seguridad-priv',
    loadChildren: () => import('./place-seguridad-priv/place-seguridad-priv.module').then( m => m.PlaceSeguridadPrivPageModule)
  },
  {
    path: 'place-reglas',
    loadChildren: () => import('./place-reglas/place-reglas.module').then( m => m.PlaceReglasPageModule)
  },
  {
    path: 'place-photos',
    loadChildren: () => import('./place-photos/place-photos.module').then( m => m.PlacePhotosPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegisterPlacePageRoutingModule {}

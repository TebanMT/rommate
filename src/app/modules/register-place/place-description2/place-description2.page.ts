import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { NavController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-place-description2',
  templateUrl: './place-description2.page.html',
  styleUrls: ['./place-description2.page.scss'],
})
export class PlaceDescription2Page implements OnInit {

  espacio = new FormGroup({
    titulo          : new FormControl('',[Validators.required]),
    descripcion_espacio        : new FormControl('',[Validators.required]),
  });

  navigationExtras: any;
  newPlace: any;

  constructor(private navController: NavController,private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      //const a = params['newPlace'].controls.espacio
      this.newPlace = params['newPlace']
      console.log(this.newPlace)
      //console.log(this.newPlace)
    });
  }

  continue(){
    this.setNavigationParams();
    this.navController.navigateForward('/place-services',this.navigationExtras);
  }

  setNavigationParams(){
    this.navigationExtras = {
      queryParams: {
          newPlace: this.newPlace,
      }
    };
  }

}

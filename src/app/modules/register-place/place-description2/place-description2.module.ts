import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlaceDescription2PageRoutingModule } from './place-description2-routing.module';

import { PlaceDescription2Page } from './place-description2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PlaceDescription2PageRoutingModule
  ],
  declarations: [PlaceDescription2Page]
})
export class PlaceDescription2PageModule {}

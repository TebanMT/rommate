import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlaceDescription2Page } from './place-description2.page';

const routes: Routes = [
  {
    path: '',
    component: PlaceDescription2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlaceDescription2PageRoutingModule {}

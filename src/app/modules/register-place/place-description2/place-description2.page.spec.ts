import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlaceDescription2Page } from './place-description2.page';

describe('PlaceDescription2Page', () => {
  let component: PlaceDescription2Page;
  let fixture: ComponentFixture<PlaceDescription2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceDescription2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlaceDescription2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

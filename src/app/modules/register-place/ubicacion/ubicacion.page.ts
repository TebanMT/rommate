import { Component, ElementRef, OnInit, ViewChild, NgZone, } from '@angular/core';
import { NavController, Platform } from "@ionic/angular";
import { GoogleMapsService } from "../../../services/google/google-maps.service";
import { Geolocation } from "@ionic-native/geolocation/ngx";
import { PopoverController } from '@ionic/angular';
import { ActivatedRoute } from "@angular/router";
import { Observable } from 'rxjs';

declare var google;

@Component({
  selector: 'app-ubicacion',
  templateUrl: './ubicacion.page.html',
  styleUrls: ['./ubicacion.page.scss'],
})
export class UbicacionPage implements OnInit {

  @ViewChild('map',{static:true, read: ElementRef}) mapElement: ElementRef;
  @ViewChild('searchbar', {static:true, read: ElementRef }) searchbar: ElementRef;
  addressElement: any = null;

  map: any;
  address = '';
  positionInfowindow: any;
  infowindow: any;
  geocoder: any;
  newPlace: any;
  navigationExtras: any;

  constructor( public maps: GoogleMapsService, public navCtrl: NavController,
    public geolocation: Geolocation,
    public zone: NgZone,
    public platform: Platform,
    public popoverCtr: PopoverController,
    public navController: NavController,
    private route: ActivatedRoute,
    ) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.newPlace = params['newPlace']
    });
  }

  ionViewDidEnter() {
    if (!!google) {
      this.initializeMap();
      this.initAutocomplete();
    } else {
      alert('Something went wrong with the Internet Connection. Please check your Internet.')
    }
  }

  initAutocomplete(): void {
    this.addressElement = this.searchbar.nativeElement.querySelector('.searchbar-input');
    this.createAutocomplete(this.addressElement).subscribe((location) => {
      let latLngObj = {'lat': location.lat(), 'long': location.lng()};
      this.getAddress(latLngObj);
      let options = {
        center: location,
        zoom: 16
      };
      this.map.setOptions(options);
    });
  }

  createAutocomplete(addressEl: any): Observable<any> {
    const autocomplete = new google.maps.places.Autocomplete(addressEl);
    autocomplete.bindTo('bounds', this.map);
    return new Observable((sub: any) => {
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
        const place = autocomplete.getPlace();
        if (!place.geometry) {
          sub.error({
            message: 'Autocomplete retgoogleurned place with no geometry'
          });
        } else {
          sub.next(place.geometry.location);
        }
      });
    });
  }

  initializeMap() {
    let that = this;
    that.currentLocation();
    this.zone.run(() => {
      var mapEle = this.mapElement.nativeElement;
      this.map = new google.maps.Map(mapEle, {
        zoom: 16,
        center: { lat: 12.971599, lng: 77.594563 },
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [{ "featureType": "water", "elementType": "geometry", "stylers": [{ "color": "#e9e9e9" }, { "lightness": 17 }] }, { "featureType": "landscape", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 20 }] }, { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [{ "color": "#ffffff" }, { "lightness": 17 }] }, { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [{ "color": "#ffffff" }, { "lightness": 29 }, { "weight": 0.2 }] }, { "featureType": "road.arterial", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 18 }] }, { "featureType": "road.local", "elementType": "geometry", "stylers": [{ "color": "#ffffff" }, { "lightness": 16 }] }, { "featureType": "poi", "elementType": "geometry", "stylers": [{ "color": "#f5f5f5" }, { "lightness": 21 }] }, { "featureType": "poi.park", "elementType": "geometry", "stylers": [{ "color": "#dedede" }, { "lightness": 21 }] }, { "elementType": "labels.text.stroke", "stylers": [{ "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 }] }, { "elementType": "labels.text.fill", "stylers": [{ "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 }] }, { "elementType": "labels.icon", "stylers": [{ "visibility": "off" }] }, { "featureType": "transit", "elementType": "geometry", "stylers": [{ "color": "#f2f2f2" }, { "lightness": 19 }] }, { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [{ "color": "#fefefe" }, { "lightness": 20 }] }, { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [{ "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 }] }],
        disableDoubleClickZoom: false,
        disableDefaultUI: true,
        zoomControl: false,
        scaleControl: true,
      });
      this.geocoder = new google.maps.Geocoder();
      // Map drag started
      this.map.addListener('dragstart', function(ev) {
        that.infowindow.close();
      });
      // Map dragging
      //Reload markers every time the map moves
      this.map.addListener('dragend', function() {
        let map_center = that.getMapCenter();
        let latLngObj = {'lat': map_center.lat(), 'long': map_center.lng() };
        that.getAddress(latLngObj);
      });

    google.maps.event.addListenerOnce(this.map, 'idle', () => {
      google.maps.event.trigger(this.map, 'resize');
      mapEle.classList.add('show-map');
    });

    google.maps.event.addListener(this.map, 'bounds_changed', () => {
      this.zone.run(() => {
        this.resizeMap();
      });
    });
  });
}

  currentLocation() {
    //this.spinner.load();
    this.geolocation.getCurrentPosition().then((position) => {
      let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
      let latLngObj = {'lat': position.coords.latitude, 'long': position.coords.longitude};
      // Display  Marker
      this.map.setCenter(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
      this.getAddress(latLngObj);
      //this.spinner.dismiss();
      return latLngObj;
    }, (err) => {
      console.log(err);
    });
  }

  getMapCenter(){
    return this.map.getCenter()
  }

  getAddress(latLngObj) {
    // Get the address object based on latLngObj
    this.geocoder.geocode(
      { location: { lat: latLngObj.lat, lng: latLngObj.long } },
      (
        results,
        status
      ) => {
        if (status === "OK") {
          if (results[0]) {
            this.getAddressComponentByPlace(results[0], latLngObj);
          } else {
            alert("No results found");
          }
        } else {
          alert("No hay dirección: " + status);
        }
      }
    );
  }

  resizeMap() {
    setTimeout(() => {
      google.maps.event.trigger(this.map, 'resize');
    }, 200);
  }

  getAddressComponentByPlace(place, latLngObj) {
    var components;
    this.address = place.formatted_address;

    components = {};

    for(var i = 0; i < place.address_components.length; i++){
      let ac = place.address_components[i];
      components[ac.types[0]] = ac.long_name;
    }

    let addressObj = {
      street: (components.street_number) ? components.street_number : 'not found',
      area: components.route,
      city: (components.sublocality_level_1) ? components.sublocality_level_1 : components.locality,
      country: (components.administrative_area_level_1) ? components.administrative_area_level_1 : components.political,
      postCode: components.postal_code,
      loc: [latLngObj.long, latLngObj.lat],
      address: this.address
    }

    const contentString =
    '<div id="content">' +
    "</div>" +
    '<h6 style = "color: black">'+this.address+'</h6>'+
    "</div>" +
    "</div>";

    this.positionInfowindow = { lat: latLngObj.lat, lng: latLngObj.long }
    this.infowindow = new google.maps.InfoWindow({
      content: contentString,
      position: this.positionInfowindow,
      disableAutoPan: true,
    });
    this.infowindow.open(this.map);
    this.newPlace.controls.direccion.patchValue({direccion_text:this.address});
    this.newPlace.controls.direccion.patchValue({lat:latLngObj.lat});
    this.newPlace.controls.direccion.patchValue({long:latLngObj.long});
    console.log(this.newPlace);
    return components;
  }

  back(){
    this.navController.navigateBack('/register-place',);
  }

  continue(){
    this.setNavigationParams();
    this.navController.navigateForward('/place-description',this.navigationExtras);
  }

  setNavigationParams(){
    this.navigationExtras = {
      queryParams: {
          newPlace: this.newPlace,
      }
    };
  }

}

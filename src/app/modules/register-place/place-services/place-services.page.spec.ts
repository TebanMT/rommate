import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlaceServicesPage } from './place-services.page';

describe('PlaceServicesPage', () => {
  let component: PlaceServicesPage;
  let fixture: ComponentFixture<PlaceServicesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceServicesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlaceServicesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

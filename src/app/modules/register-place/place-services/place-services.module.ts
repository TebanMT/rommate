import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlaceServicesPageRoutingModule } from './place-services-routing.module';

import { PlaceServicesPage } from './place-services.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    PlaceServicesPageRoutingModule
  ],
  declarations: [PlaceServicesPage]
})
export class PlaceServicesPageModule {}

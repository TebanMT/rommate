import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlaceServicesPage } from './place-services.page';

const routes: Routes = [
  {
    path: '',
    component: PlaceServicesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlaceServicesPageRoutingModule {}

import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { CatalogosService } from "../../../services/catalogos/catalogos.service";
import { NavController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-place-services',
  templateUrl: './place-services.page.html',
  styleUrls: ['./place-services.page.scss'],
})
export class PlaceServicesPage implements OnInit {

  selectedServices: any = [];

  servicios: any;
  newPlace: FormGroup;
  navigationExtras: any;

  constructor(private catalogos: CatalogosService,
    private navController: NavController,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.catalogos.getServicios().subscribe(
      res =>{
        this.servicios = res.payload;
        this.servicios.map(s => s.activo = 0);
      },
      err =>{
        alert(err);
      }
    );
    this.route.queryParams.subscribe(params => {
      this.newPlace = params['newPlace'];
    });
  }

  select(s: any){
    if (s.activo == !true) {
      this.selectedServices.push({id:s.id});
      this.newPlace.controls['servicios'].setValue(this.selectedServices);
    }else{
      const index = this.selectedServices.findIndex(i => i.id = s.id);
      if (index > -1) {
        this.selectedServices.splice(index, 1);
      }
    }
  }

  continue(){
    this.setNavigationParams();
    this.navController.navigateForward('/place-seguridad-priv',this.navigationExtras);
  }

  setNavigationParams(){
    this.navigationExtras = {
      queryParams: {
          newPlace: this.newPlace,
      }
    };
  }

}

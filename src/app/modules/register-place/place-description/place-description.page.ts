import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { CatalogosService } from "../../../services/catalogos/catalogos.service";
import { NavController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { UtilService } from "../../../services/util/util.service";

@Component({
  selector: 'app-place-description',
  templateUrl: './place-description.page.html',
  styleUrls: ['./place-description.page.scss'],
})
export class PlaceDescriptionPage implements OnInit {

  espacio = new FormGroup({
    id_tipo_espacio          : new FormControl('',[Validators.required]),
    cantidad_personas        : new FormControl('',[Validators.required]),
    precio_renta             : new FormControl('',[Validators.required]),
    fecha_disponibilidad     : new FormControl('',[Validators.required]),
  });

  espacios: any;
  ciudades: any;
  enableM: boolean = true;
  isHidden: boolean = false;

  newPlace: any;

  navigationExtras: any;

  constructor(private catalogos: CatalogosService,
    private navController: NavController,
    private route: ActivatedRoute,) { }

  ngOnInit() {
    this.catalogos.getEspacios().subscribe(
      res =>{
        this.espacios = res.payload;
      },
      error =>{
        alert(error)
      }
    );
    this.route.queryParams.subscribe(params => {
      //const a = params['newPlace'].controls.espacio
      this.newPlace = params['newPlace']
      //console.log(this.newPlace)
    });
  }

  continue(){
    if(this.isHidden){
      this.newPlace.controls['espacio'].patchValue({fecha_disponibilidad:null});
    }
    this.setNavigationParams();
    this.navController.navigateForward('/place-description2',this.navigationExtras);
  }

  showDisponibilidad(){
    this.isHidden = !this.isHidden;
  }

  setNavigationParams(){
    this.navigationExtras = {
      queryParams: {
          newPlace: this.newPlace,
      }
    };
  }

}

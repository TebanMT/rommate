import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlaceSeguridadPrivPageRoutingModule } from './place-seguridad-priv-routing.module';

import { PlaceSeguridadPrivPage } from './place-seguridad-priv.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlaceSeguridadPrivPageRoutingModule
  ],
  declarations: [PlaceSeguridadPrivPage]
})
export class PlaceSeguridadPrivPageModule {}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlaceSeguridadPrivPage } from './place-seguridad-priv.page';

describe('PlaceSeguridadPrivPage', () => {
  let component: PlaceSeguridadPrivPage;
  let fixture: ComponentFixture<PlaceSeguridadPrivPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceSeguridadPrivPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlaceSeguridadPrivPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

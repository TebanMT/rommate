import { Component, OnInit } from '@angular/core';
import { CatalogosService } from "../../../services/catalogos/catalogos.service";
import { NavController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";



@Component({
  selector: 'app-place-seguridad-priv',
  templateUrl: './place-seguridad-priv.page.html',
  styleUrls: ['./place-seguridad-priv.page.scss'],
})
export class PlaceSeguridadPrivPage implements OnInit {

  selectedSeguridadPriv: any = [];

  seguridadPriv: any;
  newPlace: any;
  navigationExtras:any;

  constructor(private catalogos: CatalogosService,
    private navController: NavController,
    private route: ActivatedRoute) { }

  ngOnInit() {
    this.catalogos.getSeguridadPriv().subscribe(
      res =>{
        this.seguridadPriv = res.payload;
        this.seguridadPriv.map(s => s.activo = 0);
      },
      err =>{
        alert(err);
      }
    );
    this.route.queryParams.subscribe(params => {
      this.newPlace = params['newPlace'];
    });
  }

  select(s: any){
    if (s.activo == !true) {
      this.selectedSeguridadPriv.push({id:s.id});
      this.newPlace.controls['seguridad_priv'].setValue(this.selectedSeguridadPriv);
    }else{
      const index = this.selectedSeguridadPriv.findIndex(i => i.id = s.id);
      if (index > -1) {
        this.selectedSeguridadPriv.splice(index, 1);
      }
    }
  }

  continue(){
    this.setNavigationParams();
    this.navController.navigateForward('/place-reglas',this.navigationExtras);
  }

  setNavigationParams(){
    this.navigationExtras = {
      queryParams: {
          newPlace: this.newPlace,
      }
    };
  }

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlaceSeguridadPrivPage } from './place-seguridad-priv.page';

const routes: Routes = [
  {
    path: '',
    component: PlaceSeguridadPrivPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlaceSeguridadPrivPageRoutingModule {}

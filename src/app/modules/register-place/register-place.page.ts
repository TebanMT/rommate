import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { NavController } from "@ionic/angular";
import { CatalogosService } from "../../services/catalogos/catalogos.service";
import { AuthService } from "../../services/auth/auth.service";

@Component({
  selector: 'app-register-place',
  templateUrl: './register-place.page.html',
  styleUrls: ['./register-place.page.scss'],
})

export class RegisterPlacePage implements OnInit {


  //Fomulario para el campo de numero de telefono
  newPlace = new FormGroup({
    espacio: new FormGroup({
      titulo: new FormControl('',[Validators.required]),
      descripcion_espacio: new FormControl('',[Validators.required]),
      precio_renta: new FormControl('',[Validators.required]),
      cantidad_personas: new FormControl('',[Validators.required]),
      diponible            : new FormControl(true,[Validators.required]),
      fecha_disponibilidad : new FormControl('',[Validators.required]),
      id_tipo_espacio: new FormControl('',[Validators.required]),
      id_usuario: new FormControl('',[Validators.required])
    }),
    direccion: new FormGroup({
      pais          : new FormControl('MEXICO',[Validators.required]),
      estado        : new FormControl('',[Validators.required]),
      ciudad        : new FormControl({value:'',disabled:true},[Validators.required]),
      colonia       : new FormControl('',[Validators.required]),
      calle         : new FormControl('',[Validators.required]),
      codigo_postal : new FormControl('',[Validators.required]),
      numero_ext    : new FormControl('',[Validators.required]),
      numero_int    : new FormControl('',[Validators.required]),
      direccion_text: new FormControl('',),
      lat           : new FormControl('',[Validators.required]),
      long          : new FormControl('',[Validators.required]),
    }),
    reglas: new FormControl(''),
    seguridad_priv: new FormControl(''),
    servicios: new FormControl(''),
    multimedia: new FormControl(''),
  });

  estados: any;
  ciudades: any;
  enableM: boolean = true;
  isHiddenNI: boolean = true;
  navigationExtras: any;

  constructor(private catalogos: CatalogosService,
    private navController: NavController, private auth: AuthService,) { }

  ngOnInit() {
    this.catalogos.getEstados().subscribe(
      res =>{
        this.estados = res.payload;
      },
      error =>{
        alert(error)
      }
    )
  }

  changeSelectedEstado(event){
    console.log(event);
    //console.log(this.newPlace)
    this.catalogos.getMunicipio(event.detail.value.id).subscribe(
      res =>{
        this.newPlace.controls['direccion'].patchValue({estado:event.detail.value.nombre});
        console.log(this.newPlace)
        this.ciudades = res.payload;
        this.enableM = false;
        this.newPlace.controls.direccion.get('ciudad').enable({emitEvent: false, onlySelf: true});
      },
      error =>{
        alert(error)
      }
    )
  }

  showNumInt(){
    this.isHiddenNI = !this.isHiddenNI;
  }

  setNavigationParams(){
    this.navigationExtras = {
      queryParams: {
          newPlace: this.newPlace,
      }
    };
  }

  continue(){
    this.newPlace.controls['espacio'].patchValue({id_usuario:this.auth.currentUserValue.payload.id});
    this.setNavigationParams();
    console.log(this.newPlace);
    this.navController.navigateForward('/ubicacion',this.navigationExtras);
  }

  compareWith(o1: any, o2: any) {
    return o1 && o2 ? o1.id === o2.id : o1 === o2;
  }

}

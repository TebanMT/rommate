import { Component, OnInit } from '@angular/core';
import { PhotoService } from "../../../services/device/photo.service";
import { ActivatedRoute } from "@angular/router";
import { EspacioRentaService } from "../../../services/espacio/espacio-renta.service";
import { NavController } from "@ionic/angular";
import { AuthService } from "../../../services/auth/auth.service";

@Component({
  selector: 'app-place-photos',
  templateUrl: './place-photos.page.html',
  styleUrls: ['./place-photos.page.scss'],
})
export class PlacePhotosPage implements OnInit {

  newPlace: any;

  constructor(public photoService: PhotoService,
    private route: ActivatedRoute,
    private espacio: EspacioRentaService,
    private navCtrl: NavController,
    private auth: AuthService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.newPlace = params['newPlace'];
    });
  }

  savePlace(){
    this.newPlace.controls['multimedia'].setValue(this.photoService.photos);
    console.log(this.newPlace.value)
    this.espacio.createEspacio(this.newPlace.value).subscribe(
      _ =>{
        this.photoService.restorePhotos();
        this.auth.updateUser('anfitrion', true);
      },err =>{
        console.log(err);
        alert(err);
      }
    )
  }

}

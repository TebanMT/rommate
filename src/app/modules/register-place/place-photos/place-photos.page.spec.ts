import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PlacePhotosPage } from './place-photos.page';

describe('PlacePhotosPage', () => {
  let component: PlacePhotosPage;
  let fixture: ComponentFixture<PlacePhotosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlacePhotosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PlacePhotosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

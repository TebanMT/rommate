import { Component, OnInit } from '@angular/core';
import { DatabaseService } from 'src/app/services/database/database.service';
import * as _ from "lodash";
import { ListRoomChat, MessageType, RoomChat } from 'src/app/services/models/chat-model';
import { NavController } from '@ionic/angular';
import { UtilService } from 'src/app/services/util/util.service';


@Component({
  selector: 'app-current',
  templateUrl: './current.page.html',
  styleUrls: ['./current.page.scss'],
})
export class CurrentPage implements OnInit {

  rooms: ListRoomChat[] = [];
  navigationExtras: any;
  public messageType = MessageType;

  constructor(
    private navController: NavController,
    private dataBase: DatabaseService,
    ) { }

  ngOnInit() {
    this.dataBase.getAllChatRoomsJson('sala','message_request').then((res) =>{
      if (res) {
        console.log(res)
        this.rooms = res;
        this.init();
      }
    }).catch((err) =>{
      console.log(err)
    });
  }

  init(){
    this.dataBase.chatRooms.subscribe((res)=>{
      console.log(res)
      if (res)
        this.rooms = res;
    })
  }

  openChatRoom(h: RoomChat){
    this.navigationExtras = {
      queryParams: {
        chatRoom: h,
      }
    };
    this.navController.navigateForward('/chat',this.navigationExtras);
  }

  formatEpoch(epoch): string {
    return UtilService.getCalendarDay(epoch);
  }

}

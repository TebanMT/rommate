import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../services/auth/auth.service";
import { EspacioRentaService } from "../../../services/espacio/espacio-renta.service";
import { ModalController, NavController } from "@ionic/angular";
import { Platform } from '@ionic/angular';
import { SearchPlacePage } from './search-place/search-place.page';

@Component({
  selector: 'app-search',
  templateUrl: './search.page.html',
  styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {

//--------------ATRIBUTOS PARA LA ANIMACION DEL SLIDE------------------------------//

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    }
  };

//------------------------------------------------------------------------------//

  espacios = [] // Lista de los espacios a mostrar
  pushes: any = [];

  constructor(public auth: AuthService,
    public espacio: EspacioRentaService,
    private navController: NavController,
    public plt: Platform, public modalController: ModalController) {}

  ngOnInit() {
    this.espacio.getEspacios()
    .subscribe(res =>{
      console.log(res)
      this.espacios = res.payload;
    },
    err =>{
      console.log(err);
    })
  }

  onChange($event) {
    console.log($event);
  }


  viewDescription(){
    this.navController.navigateForward('/full-description');
  }

  async openSearch(){
    const modal = await this.modalController.create({
      component: SearchPlacePage,
      cssClass: 'my-custom-class',
      componentProps: {}
    });
    return await modal.present();
  }

}

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { EspacioRentaService } from "../../../../services/espacio/espacio-renta.service";
import { SerachTypePage } from "../serach-type/serach-type.page";

@Component({
  selector: 'app-search-place',
  templateUrl: './search-place.page.html',
  styleUrls: ['./search-place.page.scss'],
})
export class SearchPlacePage implements OnInit {

  citiesAmount = [];
  isItemAvailable = false;
  items = [];

  constructor(private modalCtrl: ModalController, private espacio:EspacioRentaService) { }

  ngOnInit() {
    this.espacio.getCountDireccion().subscribe(res =>{
      res.payload.sort(function (a,b){return b.amount - a.amount})
      console.log(res.payload)
      this.citiesAmount = res.payload;
    },err=>{})
  }

  initializeItems(){
    this.items = this.citiesAmount;
  }


  getItems(ev: any) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the searchbar
    const val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() !== '') {
        this.isItemAvailable = true;
        this.items = this.items.filter((item) => {
            return (item.ciudad.toLowerCase().indexOf(val.toLowerCase()) > -1);
        })
    } else {
        this.isItemAvailable = false;
    }
  }

  /**
   * Close Modal
   */
  close(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  async openSearch(place: string){
    this.close();
    const modal = await this.modalCtrl.create({
      component: SerachTypePage,
      cssClass: 'my-custom-class',
      componentProps: {
        place: place,
      }
    });
    //this.close();
    return await modal.present();
  }

}

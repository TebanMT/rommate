import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';

@Component({
  selector: 'app-serach-date',
  templateUrl: './serach-date.page.html',
  styleUrls: ['./serach-date.page.scss'],
})
export class SerachDatePage implements OnInit {

  place: string;
  type: string;


  date: string = null;
  isHiddenNI: boolean = true;

  constructor(private modalCtrl: ModalController, private navController: NavController) { }

  ngOnInit() {
  }

  showNumInt(){
    this.isHiddenNI = !this.isHiddenNI;
  }

  search(){
    if(!this.isHiddenNI)
      this.date = null;
    this.close();
    this.navController.navigateForward('/tabs/search/result/'+this.place+'/'+this.type+'/'+this.date);
  }

  close(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}

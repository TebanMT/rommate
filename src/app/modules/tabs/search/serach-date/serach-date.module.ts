import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SerachDatePageRoutingModule } from './serach-date-routing.module';

import { SerachDatePage } from './serach-date.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SerachDatePageRoutingModule,
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  declarations: [SerachDatePage]
})
export class SerachDatePageModule {}

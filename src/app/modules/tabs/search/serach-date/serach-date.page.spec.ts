import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SerachDatePage } from './serach-date.page';

describe('SerachDatePage', () => {
  let component: SerachDatePage;
  let fixture: ComponentFixture<SerachDatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerachDatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SerachDatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

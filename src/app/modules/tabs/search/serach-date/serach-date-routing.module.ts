import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SerachDatePage } from './serach-date.page';

const routes: Routes = [
  {
    path: '',
    component: SerachDatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SerachDatePageRoutingModule {}

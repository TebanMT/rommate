import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SerachDatePage } from "../serach-date/serach-date.page";

@Component({
  selector: 'app-serach-type',
  templateUrl: './serach-type.page.html',
  styleUrls: ['./serach-type.page.scss'],
})
export class SerachTypePage implements OnInit {

  place: string;

  component: any;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
  }

  close(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

  async openSearch(place: string, type: string){
    this.close();
    const modal = await this.modalCtrl.create({
      component: SerachDatePage,
      cssClass: 'my-custom-class',
      componentProps: {
        place: place,
        type: type,
      }
    });
    return await modal.present();
  }

}

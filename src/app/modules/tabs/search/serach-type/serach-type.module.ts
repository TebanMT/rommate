import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SerachTypePageRoutingModule } from './serach-type-routing.module';

import { SerachTypePage } from './serach-type.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SerachTypePageRoutingModule
  ],
  declarations: [SerachTypePage]
})
export class SerachTypePageModule {}

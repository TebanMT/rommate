import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SerachTypePage } from './serach-type.page';

describe('SerachTypePage', () => {
  let component: SerachTypePage;
  let fixture: ComponentFixture<SerachTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerachTypePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SerachTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

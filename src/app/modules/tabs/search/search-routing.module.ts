import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SearchPage } from './search.page';

const routes: Routes = [
  {
    path: '',
    component: SearchPage
  },
  {
    path: 'search-place',
    loadChildren: () => import('./search-place/search-place.module').then( m => m.SearchPlacePageModule)
  },
  {
    path: 'serach-type',
    loadChildren: () => import('./serach-type/serach-type.module').then( m => m.SerachTypePageModule)
  },
  {
    path: 'serach-date',
    loadChildren: () => import('./serach-date/serach-date.module').then( m => m.SerachDatePageModule)
  },
  {
    path: 'result/:place/:type/:date',
    loadChildren: () => import('./serach-result/serach-result.module').then( m => m.SerachResultPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SearchPageRoutingModule {}

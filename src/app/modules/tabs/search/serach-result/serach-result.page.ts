import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
import { EspacioRentaService } from "../../../../services/espacio/espacio-renta.service";

@Component({
  selector: 'app-serach-result',
  templateUrl: './serach-result.page.html',
  styleUrls: ['./serach-result.page.scss'],
})
export class SerachResultPage implements OnInit {

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    autoHeight: true,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    }
  };

  lugar:string;
  type:string;
  date:string;

  results = [];

  constructor(private navCtrl: NavController, private espacios: EspacioRentaService,
    private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let lugar = this.activatedRoute.snapshot.paramMap.get("place");
    let type = this.activatedRoute.snapshot.paramMap.get("type");
    let date = this.activatedRoute.snapshot.paramMap.get("date");
    this.lugar = lugar;
    this.type = type !='null' ? ["Cuartos","Casas","Departamentos"][parseInt(type)]:"Cualquiera";
    this.date = date !='null' ? date.split("T",1)[0] : "Cualquiera";
    this.espacios.searchEspacios(lugar,type,date).subscribe(res => {
      this.results = res.payload;
    }, err=>{})
  }

  back(){
    this.navCtrl.navigateRoot('/',);
  }

}

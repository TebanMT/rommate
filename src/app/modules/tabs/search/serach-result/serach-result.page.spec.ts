import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SerachResultPage } from './serach-result.page';

describe('SerachResultPage', () => {
  let component: SerachResultPage;
  let fixture: ComponentFixture<SerachResultPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SerachResultPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SerachResultPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

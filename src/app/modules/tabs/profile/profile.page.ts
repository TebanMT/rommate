import { Component, OnInit } from '@angular/core';
import { ModalController } from "@ionic/angular";
import { SignUpPage } from "../../sign-up/sign-up.page";
import { AuthService } from "../../../services/auth/auth.service";
import { NavController } from "@ionic/angular";
import { Subscription } from 'rxjs';
import { SocketService } from 'src/app/services/socket/socket.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  usuario     : string;
  isLogin     : boolean;
  isAnfrition : boolean;
  currentUserSubscription: Subscription;

  constructor(
    public modalController: ModalController,
    public auth: AuthService,
    private navCtrl: NavController,
    private socketService: SocketService) { }

  ngOnInit() {
    this.auth.currentUser.subscribe(user =>{
      console.log("------------- User ----------")
      console.log(user)
      if (user == null) {
        this.usuario = ''
        this.isLogin = false;
        this.isAnfrition = false;
      }else{
        this.usuario = this.auth.currentUserValue.payload.nombre;
        this.isLogin = true;
        this.isAnfrition = user.payload.anfitrion ? true : false;
      }
    })
  }


  /**
   * Open Sign Up page like a modal
   */
  async openSignUp(mode: string){
    const modal = await this.modalController.create({
      component: SignUpPage,
      cssClass: 'my-custom-class',
      componentProps: { 
        login: mode == 'login' ? true : false,
      }
    });
    return await modal.present();
  }

  openRegisterPlace(){
    if (this.isAnfrition) {
      this.navCtrl.navigateRoot('/anfitrion')
    }else{
      this.navCtrl.navigateForward('/register-place')
      alert("Antes de convertirse en host, debe registrar un sitio para rentar. Por favor sigua las siguientes instrucciones.")
    }
  }

  openAnfitrion(){
    console.log("openAnfitrion")
  }

  logOut(){
    //this.socketService.disconnect();
    this.auth.logout()
  }

}

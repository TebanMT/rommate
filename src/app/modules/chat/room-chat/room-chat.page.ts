import { Component, EventEmitter, NgZone, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/services/auth/auth.service';
import { DatabaseService } from 'src/app/services/database/database.service';
import { ChatMessage, RoomChat } from 'src/app/services/models/chat-model';
import { MensajesService } from 'src/app/services/mensajes/mensajes.service';
import { UtilService } from 'src/app/services/util/util.service';
import * as _ from "lodash";

@Component({
  selector: 'app-room-chat',
  templateUrl: './room-chat.page.html',
  styleUrls: ['./room-chat.page.scss'],
})
export class RoomChatPage implements OnInit {
  @ViewChild('txtChat') txtChat: any;
  @ViewChild('content') content: any;
  messages: any[];
  chatBox: string;
  btnEmitter: EventEmitter<string>;
  chat_room: RoomChat;
  idChatRoom: string;
  receptor: string;

  private messagesSubscription: Subscription = null;

  constructor(public _zone: NgZone, public user: AuthService,
    public databaseService: DatabaseService,
    public notifyService: MensajesService,
    private activatedRoute: ActivatedRoute,) { }

  ngOnInit() {
    this.idChatRoom = this.activatedRoute.snapshot.paramMap.get("id");
    this.btnEmitter = new EventEmitter<string>();
    this.messages = [];
    this.chatBox = "";
    this.init();
  }


  ionViewWillEnter() {
    this.databaseService.getJson(this.idChatRoom, "mensajes")
      .then(messages => {
        if (messages) {
          this.messages = this.messages.concat(_.sortBy(messages, ['epoch']));
        }
        this.scrollToBottom();
        return this.databaseService.getJson(this.idChatRoom, "sala");
      }).then(res => {
        this.receptor = res?.receptor.nombre+' '+res?.receptor.apellidos
      }).catch(err =>{console.error(err)});
  }

  ionViewWillLeave() {
    console.log("destroy")
    this.messagesSubscription.unsubscribe();
  }

  init() {
    this.messagesSubscription = this.notifyService.messages.subscribe((chatMessage: ChatMessage) => {
      this._zone.run(() => {
        this.messages.push(chatMessage);
      });
      this.scrollToBottom();
    });
  }

  public sendMessage() {
    this.btnEmitter.emit("sent clicked");
    let message = this.txtChat.content;
    this.send(message);
    this.txtChat.clearInput();
  }

  async send(message) {
    let from = this.user.currentUserValue.payload.id.toString();
    let messageObj = UtilService.formatMessageRequest(message, from, this.idChatRoom);
    this.notifyService.sendMessage(this.idChatRoom, messageObj);
    this.chatBox = '';
    this.scrollToBottom();
  }

  scrollToBottom() {
    this._zone.run(() => {
      setTimeout(() => {
        this.content.scrollToBottom(300);
      });
    });
  }

  back(){
    this.databaseService.getAllChatRoomsJson('sala');
  }

}

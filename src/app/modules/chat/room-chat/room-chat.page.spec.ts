import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RoomChatPage } from './room-chat.page';

describe('RoomChatPage', () => {
  let component: RoomChatPage;
  let fixture: ComponentFixture<RoomChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoomChatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RoomChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RoomChatPageRoutingModule } from './room-chat-routing.module';

import { RoomChatPage } from './room-chat.page';
import { ChatBubbleComponent } from "../../../components/chat-bubble/chat-bubble.component";
import { ElasticTextareComponent } from "../../../components/elastic-textare/elastic-textare.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RoomChatPageRoutingModule
  ],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  entryComponents: [ChatBubbleComponent, ElasticTextareComponent],
  declarations: [RoomChatPage, ChatBubbleComponent, ElasticTextareComponent]
})
export class RoomChatPageModule {}

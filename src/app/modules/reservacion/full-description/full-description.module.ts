import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FullDescriptionPageRoutingModule } from './full-description-routing.module';

import { FullDescriptionPage } from './full-description.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FullDescriptionPageRoutingModule
  ],
  declarations: [FullDescriptionPage]
})
export class FullDescriptionPageModule {}

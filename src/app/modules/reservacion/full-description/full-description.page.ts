import { Component, OnInit, NgZone, } from '@angular/core';
import { NavController } from "@ionic/angular";
import { ActivatedRoute } from "@angular/router";
import { EspacioRentaService } from "../../../services/espacio/espacio-renta.service";
import { RoomChat } from "../../../services/models/chat-model";
import { UtilService } from "../../../services/util/util.service";
import { DatabaseService } from 'src/app/services/database/database.service';
import { DetailsPage } from "../details/details.page";
import { ModalController } from "@ionic/angular";

declare var google;

@Component({
  selector: 'app-full-description',
  templateUrl: './full-description.page.html',
  styleUrls: ['./full-description.page.scss'],
})
export class FullDescriptionPage implements OnInit {

//--------------------------------------------------------------------------------------//

  slideOpts = {
    initialSlide: 0,
    speed: 400,
    pagination: {
      el: '.swiper-pagination',
      dynamicBullets: true,
    }
  };

//-------------------------------------------------------------------------------------//

  idEspacio   : number;
  dataEspacio : any;
  navigationExtras: any;
  moreServices: string = '';

  imgMap= "http://maps.googleapis.com/maps/api/staticmap?&size=400x400&style=visibility:on \
  &style=feature:water%7Celement:geometry%7Cvisibility:on \
  &style=feature:landscape%7Celement:geometry%7Cvisibility:on \
  &markers=anchor:32,10%7Cicon:https://goo.gl/5y3S82%7CCanberra+ACT \
  &markers=anchor:topleft%7Cicon:http://tinyurl.com/jrhlvu6%7CMelbourne+VIC \
  &markers=anchor:topright%7Cicon:https://goo.gl/1oTJ9Y%7CSydney+NSW&key=AIzaSyBomWcQfBJVZN4ovCEDl6v0Bvk-G_9YPF8";

  constructor(private navCtrl: NavController, private activatedRoute: ActivatedRoute,
    private espacio: EspacioRentaService, private navController: NavController,
    private util: UtilService, private dataBase: DatabaseService,
    public modalController: ModalController) { }

  ngOnInit() {
    this.idEspacio = parseInt(this.activatedRoute.snapshot.paramMap.get("id"));
    this.espacio.getEspacio(this.idEspacio).subscribe(
      res =>{
        console.log(res)
        this.dataEspacio = res.payload;
        if (res.payload.servicios.slice(5).length != 0)
          this.moreServices = res.payload.servicios.slice(5).length.toString();
      },
      err => {
        console.log(err);
      }
    );
  }

  async contactHost(host: any){
    // crear un romm chat
    const e = await this.util.formatEmisorMessages(undefined);
    if (e != null) {
      const dataChatRoom: RoomChat = {
        id : host.id,
        receptor:{id: host.id, nombre: host.nombre, apellidos: host.apellidos, url_image: '',},
        emisor: e,
      }
      this.dataBase.setJson(host.id, dataChatRoom, "sala").then(
        res =>{
          console.log(res);
          if (res) {
            this.navController.navigateForward('/chat/'+dataChatRoom.id);
          }
        }).catch(err =>{
          console.log(err);
        });
    }else{
      this.navController.navigateForward('/chat/'+host.id);
    }
  }

  /**
   * Open Sign Up page like a modal
   */
  async openDetails(title:string,data:any, data_identifier?:string){
    const modal = await this.modalController.create({
      component: DetailsPage,
      cssClass: 'my-custom-class',
      componentProps: { 
        data: data,
        title: title,
        data_identifier: data_identifier
      }
    });
    return await modal.present();
  }

  getDate(date: string){
    let d = UtilService.getMonthYear(UtilService.getEpoch(date));
    let lower = d.toLocaleLowerCase();
    let result =d.charAt(0).toUpperCase()+lower.slice(1);
    return result
  }

  back(){
    this.navCtrl.navigateRoot('/',);
  }

}

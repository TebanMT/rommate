import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FullDescriptionPage } from './full-description.page';

const routes: Routes = [
  {
    path: '',
    component: FullDescriptionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FullDescriptionPageRoutingModule {}

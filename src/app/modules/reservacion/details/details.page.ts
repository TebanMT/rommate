import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  data: any;
  title: string;
  data_identifier: string;
  aditional_info: string;
  data_type: string;

  constructor(private modalCtrl: ModalController) { }

  ngOnInit() {
    this.data_type = typeof(this.data);
    if (this.data_identifier) {
      let lower = this.data_identifier.toLocaleLowerCase();
      this.aditional_info = this.data_identifier.charAt(0).toUpperCase()+lower.slice(1)+'s Adicionales';
    }
  }

  /**
   * Close Modal
   */
  close(){
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }

}

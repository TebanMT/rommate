import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AdminPlacesPage } from './admin-places.page';

describe('AdminPlacesPage', () => {
  let component: AdminPlacesPage;
  let fixture: ComponentFixture<AdminPlacesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminPlacesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AdminPlacesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

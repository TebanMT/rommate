import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AdminPlacesPage } from './admin-places.page';

const routes: Routes = [
  {
    path: '',
    component: AdminPlacesPage
  },
  {
    path: 'edit',
    loadChildren: () => import('./edit/edit.module').then( m => m.EditPageModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AdminPlacesPageRoutingModule {}

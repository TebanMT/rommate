import { Component, OnInit } from '@angular/core';
import { EspacioRentaService } from "../../../../services/espacio/espacio-renta.service";
import { AuthService } from "../../../../services/auth/auth.service";
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-admin-places',
  templateUrl: './admin-places.page.html',
  styleUrls: ['./admin-places.page.scss'],
})
export class AdminPlacesPage implements OnInit {

  results = [];

  constructor(private espacios: EspacioRentaService, private auth:AuthService, private navCtrl: NavController) { }

  ngOnInit() {
    this.auth.currentUser.subscribe(user =>{
      if (user != null) {
        this.espacios.searchEspaciosByAnfitrion(user.payload.id).subscribe(res => {
          console.log(res.payload);
          this.results = res.payload;
        }, err=>{})
      }
    });
  }

  delete(id){
    this.espacios.deleteEspacio(id).subscribe(res =>{
      if(res.payload == true){
        alert("Anuncio eliminado correctamente")
      }else{
        alert("Error desconocido")
      }
    },err=>{
      console.error(err)
    })
  }

  ocupado(id, status){
    this.espacios.changeActivo(id,!status).subscribe(res =>{
      if (res.payload == true) {
        alert("Estatus cambiado correctamente")
      }else{
        alert("Error desconocido")
      }
    },err =>{
      console.error(err)
    })
  }

  edit(id){
    this.navCtrl.navigateForward('/anfitrion/tabs/places/edit')
  }

  newAnuncio(){
    this.navCtrl.navigateForward('/register-place')
  }

}

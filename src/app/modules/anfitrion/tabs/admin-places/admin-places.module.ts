import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminPlacesPageRoutingModule } from './admin-places-routing.module';

import { AdminPlacesPage } from './admin-places.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AdminPlacesPageRoutingModule
  ],
  declarations: [AdminPlacesPage]
})
export class AdminPlacesPageModule {}

import { Component, OnInit } from '@angular/core';
import { ModalController, NavController } from '@ionic/angular';
import { Subscription } from 'rxjs';
import { AuthService } from "../../../../services/auth/auth.service";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  usuario     : string;
  isLogin     : boolean;
  isAnfrition : boolean;
  currentUserSubscription: Subscription;

  constructor(public modalController: ModalController, public auth: AuthService, private navCtrl: NavController) { }

  ngOnInit() {
    this.auth.currentUser.subscribe(user =>{
      console.log("------------- User ----------")
      console.log(user)
      if (user == null) {
        this.usuario = ''
        this.isLogin = false;
        this.isAnfrition = false;
      }else{
        this.usuario = this.auth.currentUserValue.payload.nombre;
        this.isLogin = true;
        this.isAnfrition = user.payload.anfitrion ? true : false;
      }
    })
  }

  changeToUsuario(){
    this.navCtrl.navigateRoot('/')
  }

  openAnfitrion(){
    console.log("openAnfitrion")
  }

  logOut(){
    //this.socketService.disconnect();
    this.auth.logout()
  }

}

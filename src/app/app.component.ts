import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from './services/auth/auth.service';
import { SocketService } from './services/socket/socket.service';
import { FCM } from 'cordova-plugin-fcm-with-dependecy-updated/ionic';
import { Router } from '@angular/router';
import { ChatMessage, MessageType } from './services/models/chat-model';
import { UtilService } from './services/util/util.service';
import { MensajesService } from "./services/mensajes/mensajes.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private auth: AuthService,
    //private fcm: FCM,
    private router: Router,
    private messageService: MensajesService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.auth.currentUser.subscribe(user =>{
        console.log("user")
        console.log(user)
        if (user != null) {
          console.log("Usuario")
          this.messageService.connect(user.payload.id);
        }
      });
      FCM.getToken().then(token => {
        console.log("token")
        console.log(token);
      });
      FCM.onTokenRefresh().subscribe(token => {
        console.log(token);
      });
      FCM.onNotification().subscribe(data => {
        if (data.wasTapped) {
          console.log('Received in background');
          //this.messageService.reciveMessage(true, chatMessage);
          //this.router.navigate(['chat', data]);
        } else {
          console.log('Received in foreground');
          //this.router.navigate(['chat/2', data]);
          //this.messageService.reciveMessage(false, chatMessage);
        }
      });
    });
  }
}
